# -*- coding: utf-8 -*-
from collections import Counter
import logging
from typing import List, Tuple
from pathlib import Path

import click
from dotenv import find_dotenv, load_dotenv
from PIL import Image
import pandas as pd
from tqdm import tqdm

from src.paths import TRAIN_CSV, TEST_CSV, TRAIN_FOLDER, TEST_FOLDER


@click.command()
def main():
    """ Runs data processing scripts to turn raw data from (../raw) into
        cleaned data ready to be analyzed (saved in ../processed).
    """
    logger = logging.getLogger(__name__)
    logger.info("making final data set from raw data")

    logger.info("creating training set")
    if not TRAIN_FOLDER.exists():
        TRAIN_FOLDER.mkdir(parents=True)

    logger.info("reading training data")
    train_df = pd.read_csv(TRAIN_CSV)
    train_entries = train_to_image(train_df)
    logger.info("writing training images")
    train_image_to_file(train_entries, TRAIN_FOLDER)

    logger.info("creating test set")
    if not TEST_FOLDER.exists():
        TEST_FOLDER.mkdir(parents=True)

    logger.info("reading test data")
    test_df = pd.read_csv(TEST_CSV)
    test_entries = test_to_image(test_df)
    logger.info("writing test images")
    test_image_to_file(test_entries, TEST_FOLDER)


def train_to_image(df: pd.DataFrame) -> List[Tuple[str, Image.Image]]:
    return [_train_to_image(df.loc[index], df.columns) for index in tqdm(df.index)]


def _train_to_image(row: pd.Series, columns: List[str]) -> Tuple[str, Image.Image]:
    label = str(row["label"])
    pixels = row[columns[1:]]
    image = to_image(pixels)
    return (label, image)


def test_to_image(df: pd.DataFrame) -> List[Image.Image]:
    return [to_image(df.loc[index]) for index in tqdm(df.index)]


def to_image(data: pd.Series) -> Image:
    pixels = data.to_numpy().astype("uint8").reshape(28, 28)
    return Image.fromarray(pixels, mode="L")


def train_image_to_file(entries: List[Tuple[str, Image.Image]], folder: Path) -> None:
    index = Counter()
    for label, image in tqdm(entries):
        name = f"{label}_{index[label]:06}.jpg"
        index.update(label)
        image.save(folder / name, "JPEG")


def test_image_to_file(entries: List[Image.Image], folder: Path) -> None:
    for label, image in enumerate(tqdm(entries)):
        name = f"{label:06}.jpg"
        image.save(folder / name, "JPEG")


if __name__ == "__main__":
    LOGGING_FORMAT = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=LOGGING_FORMAT)

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    main()

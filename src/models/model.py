# pylint: disable=arguments-differ
import torch.nn as nn
import torch


def resnet() -> nn.Module:
    module = nn.Sequential()

    module.add_module(
        "conv1", nn.Conv2d(1, 64, kernel_size=7, stride=1, padding=3, bias=False)
    )
    module.add_module("bn1", nn.BatchNorm2d(64))
    module.add_module("relu", nn.ReLU(inplace=True))
    module.add_module("maxpool", nn.MaxPool2d(kernel_size=3, stride=1, padding=1))
    # removed downsample from conv1 and maxpool
    # This has downsampled twice at this point, so 28x28 -> 7x7

    module.add_module("layer1", nn.Sequential(Basic(64, 64), Basic(64, 64)))
    channels = 64
    for i in range(2, 7):
        module.add_module(
            f"layer{i}",
            nn.Sequential(
                Basic(channels, channels * 2, downsample=True),
                Basic(channels * 2, channels * 2),
            ),
        )
        channels *= 2

    module.add_module("avgpool", nn.AdaptiveAvgPool2d(output_size=(1, 1)))
    module.add_module("reshape", Reshape())
    module.add_module("fc", nn.Linear(in_features=channels, out_features=10))

    return module


class Basic(nn.Module):
    def __init__(self, in_channels: int, out_channels: int, downsample: bool = False):
        super().__init__()

        if downsample:
            self.conv1 = _conv3x3(in_channels, out_channels, stride=2)
        else:
            self.conv1 = _conv3x3(in_channels, out_channels)

        self.bn1 = _normalize(out_channels)
        self.relu = _relu()
        self.conv2 = _conv3x3(out_channels, out_channels)
        self.bn2 = _normalize(out_channels)

        if downsample:
            self.downsample = _downsample_out(in_channels, out_channels)
        else:
            self.downsample = None

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        identity = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)

        if self.downsample is not None:
            identity = self.downsample(x)

        out += identity
        out = self.relu(out)

        return out


class Reshape(nn.Module):
    # this is hidden in the implementation of resnet
    def forward(self, x: torch.Tensor) -> torch.Tensor:
        return x.reshape(x.size(0), -1)


def _conv1x1(in_channels: int, out_channels: int, stride: int = 1) -> nn.Module:
    return nn.Conv2d(
        in_channels, out_channels, kernel_size=1, stride=stride, bias=False
    )


def _conv3x3(
    in_channels: int, out_channels: int, stride: int = 1, padding: int = 1
) -> nn.Module:
    return nn.Conv2d(
        in_channels,
        out_channels,
        kernel_size=3,
        stride=stride,
        padding=padding,
        bias=False,
    )


def _normalize(channels: int) -> nn.Module:
    return nn.BatchNorm2d(channels)


def _relu() -> nn.Module:
    return nn.ReLU(inplace=True)


def _downsample_out(in_channels: int, out_channels: int) -> nn.Module:
    return nn.Sequential(
        _conv1x1(in_channels, out_channels, stride=2), _normalize(out_channels)
    )

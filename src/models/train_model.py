from pathlib import Path
import re

from fastai.vision import ImageDataBunch, ImageList, get_image_files, rand_pad
import numpy as np

LABEL_PATTERN = re.compile(r"^([^/]+)_\d+.jpg$")


def load_data(train_folder: Path, batch_size: int = 64) -> ImageDataBunch:
    np.random.seed(2)

    def _get_label(path: Path):
        return LABEL_PATTERN.search(path.name).group(1)

    src = (
        ImageList(get_image_files(train_folder), path=train_folder, convert_mode="L")
        .split_by_rand_pct(0.2, seed=2)
        .label_from_func(_get_label)
    )
    return ImageDataBunch.create_from_ll(
        src, ds_tfms=([*rand_pad(padding=3, size=28, mode="zeros")], []), bs=batch_size
    ).normalize()

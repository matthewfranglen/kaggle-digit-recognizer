from pathlib import Path
from fastai.vision import Learner, open_image
from tqdm import tqdm
import pandas as pd


def submission(
    model: Learner, test_folder: Path, convert_mode: str = "RGB"
) -> pd.DataFrame:
    paths = sorted(test_folder.ls())
    ids = list(range(1, len(paths) + 1))
    predictions = [predict(model, path, convert_mode) for path in tqdm(paths)]
    return pd.DataFrame({"ImageId": ids, "Label": predictions}).set_index("ImageId")


def predict(model: Learner, image_path: Path, convert_mode: str = "RGB") -> str:
    """ Make predictions on a single image """
    image = open_image(image_path, convert_mode=convert_mode)
    prediction, _, _ = model.predict(image)
    return prediction

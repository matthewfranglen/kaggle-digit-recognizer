from pathlib import Path

PROJECT_ROOT = Path(__file__).resolve().parents[1]
DATA_FOLDER = PROJECT_ROOT / "data"
RAW_FOLDER = DATA_FOLDER / "raw"
PROCESSED_FOLDER = DATA_FOLDER / "processed"
TRAIN_FOLDER = PROCESSED_FOLDER / "train"
TEST_FOLDER = PROCESSED_FOLDER / "test"

TRAIN_CSV = RAW_FOLDER / "train.csv"
TEST_CSV = RAW_FOLDER / "test.csv"
SUBMISSION_CSV = PROJECT_ROOT / "submission.csv"
